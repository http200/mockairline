package com.travel.mockairline.service;

import com.travel.mockairline.domain.MockAirlineFlight;
import com.travel.mockairline.domain.MockAirlineRequest;
import com.travel.mockairline.domain.MockAirlineResponse;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
class AvailabilityServiceTest {

    @Autowired
    private AvailabilityService availabilityService;
    private MockAirlineRequest request;

    @BeforeEach
    void setUp() {
        availabilityService = new AvailabilityService("http://private-b77a9-mockairline.apiary-mock.com");
        request = new MockAirlineRequest();
        request.setOrigin("dub");
        request.setDestination("lon");
        request.setStart("2019-10-10");
        request.setEnd("2019-11-01");
        request.setPax(1);

    }

    @Test
    void requestAvailability() {
        MockAirlineResponse response = availabilityService.requestAvailability(request);
        Assert.assertTrue(!response.getAvailability().isEmpty());
        MockAirlineFlight flight = response.getAvailability().getFirst("flight");
        Assert.assertNotNull(flight.getArrivesAt());
        Assert.assertNotNull(flight.getDepartsFrom());
        Assert.assertNotNull(flight.getOperator());
        Assert.assertNotNull(flight.getFlightNumber());
    }


}