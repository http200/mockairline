package com.travel.mockairline.controller;

import com.travel.mockairline.domain.MockAirlineRequest;
import com.travel.mockairline.service.AvailabilityService;
import com.travel.mockairline.domain.MockAirlineResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MockAirlineController {

    private final AvailabilityService service;

    @Autowired
    public MockAirlineController(AvailabilityService service) {
        this.service = service;
    }


    @PostMapping("/availability")
    public MockAirlineResponse requestAvailability(@RequestBody MockAirlineRequest request) {
        return service.requestAvailability(request);
    }

    @GetMapping("/example")
    public MockAirlineRequest getSampleRequest() {
        MockAirlineRequest request = new MockAirlineRequest();
        request.setDestination("Dub");
        request.setOrigin("Lon");
        request.setStart("2019-10-01");
        request.setEnd("2019-11-01");
        request.setPax(1);
        return request;
    }
}
