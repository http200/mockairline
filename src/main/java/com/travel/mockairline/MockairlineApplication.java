package com.travel.mockairline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockairlineApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockairlineApplication.class, args);
    }

}
