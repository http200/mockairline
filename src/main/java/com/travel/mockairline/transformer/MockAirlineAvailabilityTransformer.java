package com.travel.mockairline.transformer;

import com.travel.mockairline.api.domain.generated.Availability;
import com.travel.mockairline.domain.MockAirlineFlight;
import com.travel.mockairline.domain.MockAirlineResponse;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MockAirlineAvailabilityTransformer {

    private final DateTimeFormatter airlineTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private final DateTimeFormatter outputDateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
    private final DateTimeFormatter outputTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a", Locale.ENGLISH);
    private static DecimalFormat moneyFormat = new DecimalFormat("#.##");


    public MockAirlineResponse getMockAirlineResponse(Availability availability) {


        MockAirlineResponse response = new MockAirlineResponse();

        for (Availability.Flight flight : availability.getFlight()) {
            MockAirlineFlight mockAirlineFlight = new MockAirlineFlight();
            mockAirlineFlight.setOperator(flight.getCarrierCode());
            mockAirlineFlight.setFlightNumber(flight.getFlightDesignator());
            mockAirlineFlight.setDepartsFrom(flight.getOriginAirport());
            mockAirlineFlight.setArrivesAt(flight.getDestinationAirport());

            LocalDateTime departure = LocalDateTime.parse(flight.getDepartureDate().toString(), airlineTimeFormatter);
            Map<String, String> departsOn = new HashMap<>();
            departsOn.put("date", outputDateFormatter.format(departure));
            departsOn.put("time", outputTimeFormatter.format(departure));
            mockAirlineFlight.setDepartsOn(departsOn);

            LocalDateTime arrival = LocalDateTime.parse(flight.getArrivalDate().toString(), airlineTimeFormatter);
            Map<String, String> arrivesOn = new HashMap<>();
            arrivesOn.put("date", outputDateFormatter.format(arrival));
            arrivesOn.put("time", outputTimeFormatter.format(arrival));
            mockAirlineFlight.setArrivesOn(arrivesOn);

            Duration duration = Duration.between(departure, arrival);
            long seconds = Math.abs(duration.getSeconds());
            long hours = seconds / 3600;
            long minutes = (seconds - (hours * 3600)) / 60;
            StringBuilder flightTime = new StringBuilder().append(hours).append(":").append(minutes);
            mockAirlineFlight.setFlightTime(flightTime.toString());


            Map<String, Object> first = new HashMap<>();
            Map<String, Object> business = new HashMap<>();
            Map<String, Object> economy = new HashMap<>();

            for (Availability.Flight.Fares.Fare fare : flight.getFares().getFare()) {
                String currencyValue = fare.getBasePrice().split(" ")[0];
                String ticketValue = fare.getBasePrice().split(" ")[1];
                Map<String, Object> ticket = new HashMap<>();
                ticket.put("currency", currencyValue);
                ticket.put("amount", moneyFormat.format(Double.valueOf(ticketValue)));
                String bookingFeeValue = fare.getFees().split(" ")[1];
                Map<String, Object> bookingFee = new HashMap<>();
                bookingFee.put("currency", currencyValue);
                bookingFee.put("amount", moneyFormat.format(Double.valueOf(bookingFeeValue)));
                String taxValue = fare.getTax().split(" ")[1];
                Map<String, Object> tax = new HashMap<>();
                tax.put("currency", currencyValue);
                tax.put("amount", moneyFormat.format(Double.valueOf(taxValue)));
                switch (fare.getClazz()) {
                    case economyClass:
                        economy.put("ticket", ticket);
                        economy.put("bookingFee", bookingFee);
                        economy.put("tax", tax);
                        break;
                    case businessClass:
                        business.put("ticket", ticket);
                        business.put("bookingFee", bookingFee);
                        business.put("tax", tax);
                        break;
                    case firstClass:
                        first.put("ticket", ticket);
                        first.put("bookingFee", bookingFee);
                        first.put("tax", tax);
                        break;
                }

            }
            mockAirlineFlight.getFarePrices().put("first", first);
            mockAirlineFlight.getFarePrices().put("business", business);
            mockAirlineFlight.getFarePrices().put("economy", economy);


            response.addAvailability(mockAirlineFlight);
        }
        return response;
    }

    private final String economyClass = "YIF";
    private final String businessClass = "CIF";
    private final String firstClass = "FIF";
}
