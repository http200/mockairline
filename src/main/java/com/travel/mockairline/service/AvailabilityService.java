package com.travel.mockairline.service;

import com.travel.mockairline.domain.MockAirlineRequest;
import com.travel.mockairline.transformer.MockAirlineAvailabilityTransformer;
import com.travel.mockairline.api.domain.generated.Availability;
import com.travel.mockairline.domain.MockAirlineResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class AvailabilityService {


    private final String apiUrl;

    public AvailabilityService (@Value("${apiUrl}") String apiUrl){
        this.apiUrl = apiUrl;
    }


        public MockAirlineResponse requestAvailability(MockAirlineRequest request) {
        Availability availability = requestAvailability(request.getOrigin(), request.getDestination(),
                request.getStart(), request.getEnd(), request.getPax());
        MockAirlineAvailabilityTransformer transformer = new MockAirlineAvailabilityTransformer();
        MockAirlineResponse response = transformer.getMockAirlineResponse(availability);
        return response;
    }


    public Availability requestAvailability(String origin, String destination, String start, String end, int pax) {
        StringBuilder requestPath = new StringBuilder().append("/flights");
        requestPath.append("/").append(origin.toUpperCase());
        requestPath.append("/").append(destination.toUpperCase());
        requestPath.append("/").append(start);
        requestPath.append("/").append(end);
        requestPath.append("/").append(pax);
        URI targetUrl = UriComponentsBuilder.fromUriString(apiUrl)
                .path(requestPath.toString())
                .build()
                .toUri();
        Availability response = getResponse(targetUrl);
        return response;
    }

    public Availability getResponse(URI targetUrl) {
        RestTemplate restTemplate = new RestTemplate();
        Availability response = restTemplate.getForObject(targetUrl, Availability.class);
        return response;
    }
}
