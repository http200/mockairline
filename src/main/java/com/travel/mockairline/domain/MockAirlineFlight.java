package com.travel.mockairline.domain;

import java.util.LinkedHashMap;
import java.util.Map;

public class MockAirlineFlight {

    private String operator;
    private String flightNumber;
    private String departsFrom;
    private String arrivesAt;
    private Map<String, String> departsOn;
    private Map<String, String> arrivesOn;
    private String flightTime;
    private LinkedHashMap<String, Object> farePrices = new LinkedHashMap<>();

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartsFrom() {
        return departsFrom;
    }

    public void setDepartsFrom(String departsFrom) {
        this.departsFrom = departsFrom;
    }

    public String getArrivesAt() {
        return arrivesAt;
    }

    public void setArrivesAt(String arrivesAt) {
        this.arrivesAt = arrivesAt;
    }

    public Map<String, String> getDepartsOn() {
        return departsOn;
    }

    public void setDepartsOn(Map<String, String> departsOn) {
        this.departsOn = departsOn;
    }

    public Map<String, String> getArrivesOn() {
        return arrivesOn;
    }

    public void setArrivesOn(Map<String, String> arrivesOn) {
        this.arrivesOn = arrivesOn;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public LinkedHashMap<String, Object> getFarePrices() {
        return farePrices;
    }

    public void setFarePrices(LinkedHashMap<String, Object> farePrices) {
        this.farePrices = farePrices;
    }
}
