package com.travel.mockairline.domain;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class MockAirlineResponse {


    MultiValueMap<String, MockAirlineFlight> availability = new LinkedMultiValueMap<>();

    public MultiValueMap<String, MockAirlineFlight> getAvailability() {
        return availability;
    }

    public void addAvailability(MockAirlineFlight mockAirlineFlight) {
        this.availability.add("flight", mockAirlineFlight);
    }


}
